<?php

namespace Drupal\timeline_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Timeline Block.
 *
 * @Block(
 *   id="timeline_block",
 *   admin_label = @Translation("Timeline Block"),
 * )
 */
class TimelineBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Creates a Timeline Block instance.
   *
   * This constructor is used to initialize a Timeline Block plugin. It takes
   * the configuration array, plugin ID, plugin definition, and the entity
   * storage service as arguments to set up the plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   *   This includes settings or any other data necessary for configuring the
   *   specific instance of the plugin, such as its state or user preferences.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   *   The unique identifier for the plugin instance, used to distinguish
   *   between different plugin types.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *   This contains the full configuration or metadata that defines the
   *   behavior of the plugin. It might include properties like the
   *   plugin's label, its settings, or other specialized configurations.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage service.
   *   This service allows interaction with the storage of entities, such
   *   as loading, saving, or deleting entities. It is injected into the
   *   constructor to enable operations on entities, which might be used
   *   in the context of the Timeline Block.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $entity_storage, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileStorage = $entity_storage;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'timeline_block' => [],
    ];
  }

  /**
   * Overrides \Drupal\Core\Block\BlockBase::blockForm().
   *
   * Adds body and description fields to the block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $items = [];
    if (isset($config['timeline_data'])) {
      $items = $config['timeline_data'];
      if ($items != '') {
        $items = json_decode($items);
      }
    }

    // Timeline header.
    $form['timeline_header'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Timeline header content'),
      '#description' => $this->t('Timeline header content'),
      '#default_value' => $config['timeline_header'] ?? '',
      '#format' => $config['timeline_header_format'] ?? 'basic_html',
    ];

    $form['#tree'] = TRUE;

    $form['items_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Timeline items'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    if (!$form_state->has('num_items')) {
      if (is_array($items)) {
        $count_items = count($items);
      }
      else {
        $count_items = 0;
      }
      $form_state->set('num_items', $count_items);
    }
    $number_of_items = $form_state->get('num_items');

    for ($i = 0; $i < $number_of_items; $i++) {

      $is_active_row = $form_state->get("row_" . $i);

      if ($is_active_row != 'inactive') {

        $j = $i + 1;
        $form['items_fieldset']['items'][$i] = [
          '#type' => 'details',
          '#title' => $this->t('Timeline @j', ['@j' => $j]),
          '#prefix' => '<div id="items-fieldset-wrapper">',
          '#suffix' => '</div>',
          '#open' => TRUE,
        ];
        $form['items_fieldset']['items'][$i]['title'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Title'),
          '#description' => $this->t('Timeline item title'),
          '#default_value' => $items[$i]->title ?? '',
        ];
        $form['items_fieldset']['items'][$i]['time'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Time'),
          '#description' => $this->t('Timeline item time pase'),
          '#default_value' => $items[$i]->time ?? '',
        ];
        $form['items_fieldset']['items'][$i]['description'] = [
          '#type' => 'text_format',
          '#title' => $this->t('Description'),
          '#description' => $this->t('Timeline item description'),
          '#default_value' => $items[$i]->description ?? '',
          '#format' => $items[$i]->description_format ?? 'basic_html',
        ];
        $form['items_fieldset']['items'][$i]['weight'] = [
          '#type' => 'number',
          '#min' => 1,
          '#max' => 120,
          '#title' => $this->t('Timeline item order weight'),
          '#description' => $this->t('The weight field can be used to provide customized sorting of timeline item.'),
          '#default_value' => $items[$i]->weight ?? $j,
        ];
        $form['items_fieldset']['items'][$i]['remove_single_item'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove item'),
          '#name' => 'row_' . $i,
          '#submit' => [[$this, 'removeItemCallback']],
          '#ajax' => [
            'callback' => [$this, 'addmoreCallback'],
            'wrapper' => 'items-fieldset-wrapper',
          ],
          '#button_type' => 'danger',
        ];
      }
    }

    $form['items_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['items_fieldset']['actions']['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add timeline'),
      '#submit' => [[$this, 'addOne']],
      '#ajax' => [
        'callback' => [$this, 'addmoreCallback'],
        'wrapper' => 'items-fieldset-wrapper',
      ],
      '#button_type' => 'primary',
    ];

    $form['timeline_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Timeline configuration'),
      '#description' => $this->t('Make adjustments such as how many items will appear according to screen sizes and other configurations.'),
      '#prefix' => '<div id="items-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['timeline_configuration']['timeline_layout'] = [
      '#type' => 'select',
      '#title' => $this->t('Timeline layout'),
      '#description' => $this->t('Timeline layout'),
      '#options' => [
        '1' => $this->t('Layout 1'),
        '2' => $this->t('Layout 2'),
        '3' => $this->t('Layout 3'),
        '4' => $this->t('Layout 4'),
        '5' => $this->t('Layout 5'),
        '6' => $this->t('Layout 6'),
        '7' => $this->t('Layout 7'),
        '8' => $this->t('Layout 8'),
        '9' => $this->t('Layout 9'),
        '10' => $this->t('Layout 10'),
      ],
      '#default_value' => $config['timeline_layout'] ?? '1',
    ];

    return $form;
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeItemCallback(array &$form, FormStateInterface $form_state) {
    $button_clicked = $form_state->getTriggeringElement()['#name'];

    $form_state->set($button_clicked, 'inactive');

    $form_state->setRebuild();

  }

  /**
   * Adds one to the current number of items stored in the form state.
   *
   * This function retrieves the current value of 'num_items' from the
   * form state, increments it by 1, and then updates the form state with
   * the new value. It also triggers a form rebuild to reflect the changes.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface object.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    $add_button = $number_of_items + 1;
    $form_state->set('num_items', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Callback function for adding more items in the form via AJAX.
   *
   * This callback is triggered during an AJAX request. It returns a specific
   * part of the form (the 'items_fieldset' within the 'settings' fieldset) to
   * be updated in the response, allowing the form to update dynamically.
   *
   * @param array $form
   *   The entire form array, which is passed to the callback function
   *   during AJAX.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface object, which holds the current form state.
   *
   * @return array
   *   The updated part of the form to be returned for the AJAX response.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    // The form passed here is the entire form, not the subform that is
    // passed to non-AJAX callback.
    return $form['settings']['items_fieldset'];
  }

  /**
   * Callback function for removing an item from the form via AJAX.
   *
   * This callback is triggered during an AJAX request to remove an item
   * from a list or group. It retrieves the current number of items, decreases
   * the count by one if possible, updates the form state, and triggers a
   * form rebuild.
   *
   * @param array $form
   *   The entire form array, which is passed to the callback function
   *   during AJAX.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface object, which holds the current form state.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    if ($number_of_items > 1) {
      $remove_button = $number_of_items - 1;
      $form_state->set('num_items', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * Callback function for removing a single item from the form via AJAX.
   *
   * This callback is triggered during an AJAX request to remove a single item
   * from a list or group. It retrieves the current number of items, decreases
   * the count by one (if more than one item exists), updates the form state,
   * and triggers a form rebuild to reflect the changes.
   *
   * @param array $form
   *   The entire form array, which is passed to the callback function
   *   during AJAX.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface object, which holds the current form state.
   */
  public function removeCallbackSingle(array &$form, FormStateInterface $form_state) {
    $number_of_items = $form_state->get('num_items');
    if ($number_of_items > 1) {
      $remove_button = $number_of_items - 1;
      $form_state->set('num_items', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $timeline_header = $this->configuration['timeline_header'] ?? '';
    $timeline_data_config = $this->configuration['timeline_data'] ?? '';
    $old_timeline_image_ids = timeline_block_get_timeline_description_images($timeline_data_config);
    $current_timeline_image_ids = [];
    $current_header_image_ids = [];
    $items = [];

    $values = $form_state->getValues();
    foreach ($values as $key => $value) {

      if ($key === 'items_fieldset') {
        if (isset($value['items']) && !empty($value['items'])) {
          $items = $value['items'];

          usort($items, function ($x, $y) {
            if (is_numeric($x['weight']) && is_numeric($y['weight'])) {
              return $x['weight'] - $y['weight'];
            }
          });

          foreach ($items as $key => $item) {
            if (trim($item['description']['value']) === '') {
              unset($items[$key]);
            }
            else {
              if (!is_numeric($item['weight'])) {
                $items[$key]['weight'] = 1;
              }

              $description_value = $items[$key]['description']['value'] ?? '';
              $description_format = $items[$key]['description']['format'] ?? 'basic_html';

              $items[$key]['description'] = $description_value;
              $items[$key]['description_format'] = $description_format;
              $new_img_ids = timeline_block_get_timeline_editor_files($description_value);

              $current_timeline_image_ids = array_merge($current_timeline_image_ids, $new_img_ids);
            }
          }

          $timeline_data = array_values($items);
          $timeline_data = json_encode($timeline_data);

          $this->configuration['timeline_data'] = $timeline_data;
        }
        else {
          $this->configuration['timeline_data'] = '';
        }
      }
    }

    if ($form_state->getValue('timeline_configuration')) {
      $timeline_configuration = $form_state->getValue('timeline_configuration');
      $this->configuration['timeline_layout'] = $timeline_configuration['timeline_layout'];
    }

    $timeline_header_content = $values['timeline_header']['value'] ?? '';
    $this->configuration['timeline_header'] = $timeline_header_content;
    $this->configuration['timeline_header_format'] = $values['timeline_header']['format'] ?? 'basic_html';

    $old_header_image_ids = timeline_block_get_timeline_editor_files($timeline_header);
    $current_header_image_ids = timeline_block_get_timeline_editor_files($timeline_header_content);

    $all_old_image_ids = array_merge($old_timeline_image_ids, $old_header_image_ids);
    $all_new_image_ids = array_merge($current_timeline_image_ids, $current_header_image_ids);

    // Remove old images.
    $result = array_diff($all_old_image_ids, $all_new_image_ids);

    if (!empty($result)) {
      foreach ($result as $key => $author_image_id) {
        $file = $this->fileStorage->load($author_image_id);
        $file->setTemporary();
        $file->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Retrieve block configurations.
    $timeline_data_config = $this->configuration['timeline_data'] ?? '';
    $timeline_layout = $this->configuration['timeline_layout'] ?? 1;
    $timeline_header_content = $this->configuration['timeline_header'] ?? '';
    $timeline_header_format = $this->configuration['timeline_header_format'] ?? 'full_html';
    $timeline_settings = [
      'timeline_layout' => $timeline_layout,
    ];

    $timeline_header_data = [
      '#type' => 'processed_text',
      '#text' => $timeline_header_content,
      '#format' => $timeline_header_format,
    ];

    $timeline_header = $this->renderer->renderPlain($timeline_header_data);

    $timeline_data = [];

    // Process and format the timeline data if available.
    if (!empty($timeline_data_config)) {
      $timeline_data_raw = json_decode($timeline_data_config);
      if ($timeline_data_raw && is_array($timeline_data_raw)) {
        foreach ($timeline_data_raw as $item) {
          // Check if description and format exist before processing.
          $formatted_description = '';
          if (!empty($item->description) && !empty($item->description_format)) {
            $formatted_description = [
              '#type' => 'processed_text',
              '#text' => $item->description,
              '#format' => $item->description_format ?? 'full_html',
            ];
          }

          // Add processed data to the timeline array.
          $timeline_data[] = [
            'title' => $item->title ?? '',
            'time' => $item->time ?? '',
            'weight' => $item->weight ?? 0,
            'description' => $formatted_description
              ? $this->renderer->renderPlain($formatted_description)
              : '',
          ];
        }
      }
    }

    // Build the render array.
    $build = [
      'timeline' => [
        '#theme' => 'timeline_block',
        '#timeline_data' => $timeline_data,
        '#timeline_settings' => $timeline_settings,
        '#timeline_header' => $timeline_header,
      ],
      '#attached' => [
        'library' => ['timeline_block/timeline_block'],
      ],
    ];

    return $build;
  }

}
